# QBSVC - Query Builder Service

## baseline

The baseline for this service is just a docker container that makes a
connection to the Sage500 database (in this case the Test DB).

The docker container is based on Ubuntu 20.04 and installs the Microsoft
SQL Client for linux.  The odbcinst.cfg contains several modifications
that fix a problem with pyodbc using that driver.  

The code sql_test.py just makes a connection and queries the table to return
the number of QB header records it finds.

To run it, use the following commands on a machine that is on the arc.local network (or tied in using the VPN)

```
docker-composer up -d --build
```
then
```
docker-composer run qbsvc-api /bin/bash
```
from there, run
```python
python sql_test.py
```
Which should give the following result
```
$ docker-compose run qbsvc-api /bin/bash
Creating qbsvc_qbsvc-api_run ... done
root@b33e059370a2:/code# python --version
Python 3.8.5
root@b33e059370a2:/code# ls
docker  docker-compose.yml  qbservice  readme.md  sql_test.py
root@b33e059370a2:/code# python sql_test.py 
DRIVER=ODBC Driver 17 for SQL Server;SERVER=192.168.1.79;PORT=1433;DATABASE=ARC_test_9_12_18;UID=python_automation;PWD=******;
There are 8435 rows in the QueryHeader table
root@b33e059370a2:/code#
```
 
