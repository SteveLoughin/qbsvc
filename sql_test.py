# sql_test.py #

import pyodbc
DB = {
    'driver': 'ODBC Driver 17 for SQL Server',
    'server': '192.168.1.79', # 'ARCVM09.arc.local', # ip works inside docker
    'port': 1433,
    'database': 'ARC_test_9_12_18',
    'uid': 'python_automation',
    'pwd': '$neaky$nak3',
    }

def get_connect_str():
    cs = ''
    for key in DB:
        cs += f"{key.upper()}={DB[key]};"
    return cs

cs = get_connect_str()
print(cs)
con = pyodbc.connect(cs)
cursor = con.cursor()
cursor.execute('select count(*) from dbo.ARC_QB_QueryHeader;')
rows = cursor.fetchall()
for row in rows:
    print(f"There are {row[0]} rows in the QueryHeader table")

    
